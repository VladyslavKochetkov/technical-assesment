# Technical Assessment

#### Vladyslav Kochetkov (July 24, 2019)

# Libraries Used

- ws https://www.npmjs.com/package/ws
  - Used to handle web socket messages between client and NodeJS server
- ts https://www.npmjs.com/package/typescript
  - Used to handle typing inside project to decrease risk of improper code
- ts-node-dev https://www.npmjs.com/package/ts-node-dev
  - Is used in development process for make the typescript compilation process automated
- @types/_ https://www.npmjs.com/package/@types/_
  - Typed for pre-existing or installed libraries, these have no impact on code execution
- Bentley Ottman Sweepline https://www.npmjs.com/package/bentley-ottman-sweepline
  - An algorithm that calculated if lines are overlapped by getting the start point of the segment, organizing them in an array, and going from left to right and checking if their start index is within another lines x axis range

# Execution

- Have Node.JS and NPM installed on your system
- Go into the project folder using terminal and run `npm install`
- In the same directoy execute the following `npm start`
- The application is now avaiable on `localhost:8000`

# Corner Cases

## INITIALIZE

- Socket could connect but not send the INITIALIZE socket, which would result in server errors because game store would not be decalred

## Point Click

### Node Press (All Cases)

- Could be outside the bound, limit press between 0 and 3 [✓]
- Press could happen on an already selected node [✓]

### Node Press (Start Node Select)

- Press could happen outside of a 1 node radius of HEAD or TAIL [✓]

### Node Press (End Node Select)

- Press could happen outside of a 1 node radius of PREVIOUS SELECTED node [✓]
- Press could force a line to overlap with an already existing line [✓]

## Errors

- Message is not proper JSON
- ID is not of type number
