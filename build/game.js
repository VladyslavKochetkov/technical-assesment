"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const point_1 = require("./point");
// Map should be private, all point should be accessed via (x,y) functions
class Game {
    constructor() {
        // Generate a random game ID
        this.id = Math.floor(Math.random() * 100000);
        // Store nodes in 2D array of points
        this.map = [];
        // Which node should be replaces, this depends on which node is clicked on, tail or head
        this.toReplace = "startNode";
        // Keep track of player turns
        this.turn = 1;
        // Store line segments that are generated in order to check for oberlaps
        this.lineSegments = [];
        // Generate an X,Y array plot to keep the points
        for (let x = 0; x < 4; x++) {
            this.map[x] = [];
            for (let y = 0; y < 4; y++)
                this.map[x][y] = new point_1.Point(x, y, this);
        }
    }
    // Return the point base on (x,y)
    getPoint(x, y) {
        return this.map[x][y];
    }
    // Toggle turns between 1 & 2
    nextTurn() {
        this.turn = this.turn === 1 ? 2 : 1;
        return this.turn;
    }
    // Select a node
    selectNode(x, y, dontVerify = false) {
        const point = this.getPoint(x, y);
        if (point.isSelected && dontVerify)
            throw "Point has already been selected";
        point.select();
        this.selectedNode = point;
        return point;
    }
    // Generate a server response, as string for WS to send
    generateResponse(id, msg, bodyMsg, newLine = null) {
        const res = {
            id,
            msg,
            body: {
                newLine: newLine,
                heading: `Player ${this.turn}`,
                message: bodyMsg
            }
        };
        // console.log(res);
        return JSON.stringify(res);
    }
}
exports.Game = Game;
