"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const findIntersections = require("bentley-ottman-sweepline");
// import { store } from "./store";
exports.nodeClick = (game, msg) => {
    const { x, y } = msg.body;
    console.log(`Click x: ${x} y: ${y}`);
    // This satifies the corner case (✓) Symbol in Excel [Quick Guide])
    if (x < 0 || x > 3 || y < 0 || y > 3)
        return game.generateResponse(msg.id, "INVALID_START_NODE", "Not a valid position");
    // We can filter if this is the first or second press based on if there is already a selected done
    if (!game.selectedNode) {
        // handleFirstClick() returns a string if there is an error, if a string exists, return a invalid start and the message
        const invalidMove = handleFirstClick(game, x, y);
        if (invalidMove) {
            return game.generateResponse(msg.id, "INVALID_START_NODE", invalidMove);
        }
        else {
            return game.generateResponse(msg.id, "VALID_END_NODE", "Select a second node");
        }
    }
    else {
        // see handleFirstClick comment
        const invalidMove = handleSecondClick(game, x, y);
        if (invalidMove) {
            return game.generateResponse(msg.id, "INVALID_END_NODE", invalidMove);
        }
        else {
            // checkAllPoints checks if there are any more valid moves from a point
            // so we have to check the start, and end node
            const startOK = !game.startNode.checkAllPointsAround();
            const endOK = !game.endNode.checkAllPointsAround();
            // console.log(startOK, endOK);
            // If there are no more
            if (startOK && endOK) {
                // Next turn to return to past turn because handleSecondClick automatically goes to next turn
                game.nextTurn();
                return game.generateResponse(msg.id, "GAME_OVER", `Player ${game.turn} wins!`, {
                    start: { ...game.selectedNode.getCords() },
                    end: {
                        x,
                        y
                    }
                });
            }
            // If game isn't over continue like usual
            const res = game.generateResponse(msg.id, "VALID_END_NODE", null, {
                start: { ...game.selectedNode.getCords() },
                end: {
                    x,
                    y
                }
            });
            game.selectedNode = undefined;
            return res;
        }
    }
};
function handleFirstClick(game, x, y) {
    try {
        // If there is not a start node (this implies first move), select the node and set to startNode
        if (!game.startNode) {
            const point = game.selectNode(x, y, true);
            point.select();
            game.startNode = point;
            return "";
        }
        else {
            // Get the nodes of the current start and end
            const startNodeCords = game.startNode.getCords();
            const endNodeCords = game.endNode.getCords();
            // Only allow to start from edges of line, so current select point must be equal to startNode or endNode
            if ((startNodeCords.x === x && startNodeCords.y === y) ||
                (endNodeCords.x === x && endNodeCords.y === y)) {
                // Select which one to replace later
                if (startNodeCords.x === x && startNodeCords.y === y)
                    game.toReplace = "startNode";
                else
                    game.toReplace = "endNode";
                // Select node
                const point = game.selectNode(x, y);
                point.select();
                return "";
            }
            else {
                return "Invalid select. Try again.";
            }
        }
    }
    catch (e) {
        return e;
    }
}
function handleSecondClick(game, x, y) {
    try {
        // Get the select node from firstClick
        const firstSelected = game.selectedNode;
        // Calculate the degree of the new angle
        const degree = firstSelected.getDegree(x, y);
        // Degree must be in a 45 degree increment, otherwise node doesnt either \ / | --
        if (degree % 45 === 0) {
            // Go through each line of nodes and check they aren't selected, if they are reject and show Invalid
            if (!stepThroughNodes(game, x, y, degree)) {
                game.selectedNode = undefined;
                return "Invalid select. Try again.";
            }
            // Start the endNode if it isn't before (generally first mode)
            if (!game.endNode)
                game.endNode = game.getPoint(x, y);
            else
                game[game.toReplace] = game.getPoint(x, y); // Replace with new if it exists (this is either startNode or endNode)
            // Add new segment to segment list
            game.lineSegments.push([
                [firstSelected.getCords().x, -firstSelected.getCords().y],
                [x, -y]
            ]);
            game.nextTurn();
            return "";
        }
        else {
            // Node is not 45 deg so deselect everything
            game.selectedNode = undefined;
            firstSelected.isSelected = false;
            // If its invalid on first move, remove startNode
            if (!game.endNode) {
                game.startNode = undefined;
            }
            return "Invalid select. Try again.";
        }
    }
    catch (e) {
        return e;
    }
}
function stepThroughNodes(game, toX, toY, degree) {
    let xIncrement = 0;
    let yIncrement = 0;
    // Set increments based on degree angels
    switch (degree) {
        case 0:
        case -0:
            xIncrement = 1;
            break;
        case 45:
            xIncrement = 1;
            yIncrement = -1;
            break;
        case 90:
            yIncrement = -1;
            break;
        case 135:
            xIncrement = -1;
            yIncrement = -1;
            break;
        case 180:
            xIncrement = -1;
            break;
        case 225:
            xIncrement = -1;
            yIncrement = 1;
            break;
        case 270:
            yIncrement = 1;
            break;
        case 315:
            yIncrement = 1;
            xIncrement = 1;
    }
    // getCurrentNode
    const selectedNode = game.selectedNode;
    //Get its points
    let { x, y } = selectedNode.getCords();
    const allVisited = [];
    do {
        x += xIncrement;
        y += yIncrement;
        // Check if it selected, and if its not add to a list, if node is selected return WITHOUT selecting past nodes
        const point = game.getPoint(x, y);
        if (point.isSelected) {
            return false;
        }
        allVisited.push(point);
    } while (x !== toX || y !== toY);
    // Check if there is an overlap when adding the new line
    const test = [
        ...game.lineSegments,
        [[selectedNode.getCords().x, -selectedNode.getCords().y], [toX, -toY]]
    ];
    const intersectionInTest = findIntersections(test).filter(
    // Filter all points away that are not decimal
    (point) => point[0] % 1 !== 0 || point[1] % 1 !== 0);
    // If there is an point that overlaps, return invalid move
    if (intersectionInTest.length !== 0)
        return false;
    // Select all nodes because line is valid
    allVisited.forEach(point => point.select());
    return true;
}
