"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const ws_1 = require("ws");
// import { init } from "./init";
const nodeClick_1 = require("./nodeClick");
const store_1 = require("./store");
const PORT = process.env.PORT || 8000;
// Clear console for easier dev mode
console.clear();
// Basic file serving
const server = http_1.default.createServer(async (req, res) => {
    if (!req.url)
        return res.end("Invalid Request");
    const filePath = path_1.default.join(__dirname, "..", "static", req.url === "/" ? "/index.html" : req.url);
    var ext = path_1.default.extname(filePath);
    let contentType;
    switch (ext) {
        case ".js":
            contentType = "text/javascript";
            break;
        case ".css":
            contentType = "text/css";
            break;
        default:
            contentType = "text/html";
            break;
    }
    fs_1.default.readFile(filePath, (err, data) => {
        if (err) {
            console.log(err);
            if (err.code === "ENOENT") {
                res.writeHead(404, { "Content-Type": "text/html" });
                return res.end("File not found", "utf-8");
            }
            res.writeHead(500, { "Content-Type": "text/html" });
            return res.end("Unknown Internal Error", "utf-8");
        }
        res.writeHead(200, { "Content-Type": contentType });
        return res.end(data, "utf-8");
    });
});
server.on("clientError", (err, socket) => {
    socket.end("HTTP/1.1 400 Bad Request\r\n\r\n");
});
const wss = new ws_1.Server({ server });
wss.on("connection", (ws) => {
    // generate a game ID to be used to read/write into the game store
    let game;
    console.log(`New connection`);
    //listen of messages on the WebSocket
    ws.on("message", (message) => {
        //parse messages into JSON as incomingMessage type
        const msg = JSON.parse(message);
        //switch through possible msg types
        switch (msg.msg) {
            case "NODE_CLICKED":
                // console.log(msg);
                // Run node click function and send its response to client
                ws.send(nodeClick_1.nodeClick(game, msg));
                // console.table(game.map.map(map => map.map(point => point.isSelected)));
                break;
            case "INITIALIZE":
                // console.log("INIT\n", msg);
                // Generate new game and store it to WS session scope
                game = store_1.store.generateGame();
                console.log(`Generating object with ID ${game.id}`);
                // Send OK
                return ws.send(game.generateResponse(msg.id, "INITIALIZE", "Waiting for player 1 to make a move"));
            case "ERROR":
                // Log any errors
                return console.log(msg);
            default:
                // Log any other WS msg types that are unknown
                console.log("This technically shouldn't happen\n", msg);
                break;
        }
    });
});
// Listen on port
server.listen(PORT, () => {
    console.log(`LISTENING ON POINT ${PORT}`);
});
