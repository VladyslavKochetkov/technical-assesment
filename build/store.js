"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const game_1 = require("./game");
// Stores make games
class Store {
    constructor() {
        // Store all games in a map [hash map]
        this.games = {};
        // Returns a game, but this really shouldn't be used as games should be stored in connection session
        this.getGame = (id) => this.games[id];
        // Generate game and save to map
        this.generateGame = () => {
            const game = new game_1.Game();
            this.games[game.id] = game;
            return game;
        };
        // Delete specific key
        this.deleteGame = (id) => {
            delete this.games[id];
        };
        // Delete all games by generating new games object
        this.deleteAll = () => {
            this.games = {};
        };
    }
}
exports.store = new Store();
