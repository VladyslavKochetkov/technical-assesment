import http from "http";
import fs from "fs";
import path from "path";
import { Server, default as WS } from "ws";
// import { init } from "./init";
import { nodeClick } from "./nodeClick";
import { store } from "./store";
import { Game } from "./game";

const PORT = process.env.PORT || 8000;

// Clear console for easier dev mode
console.clear();

// Basic file serving
const server = http.createServer(async (req, res) => {
  if (!req.url) return res.end("Invalid Request");
  const filePath = path.join(
    __dirname,
    "..",
    "static",
    req.url === "/" ? "/index.html" : req.url
  );
  var ext = path.extname(filePath);
  let contentType: string;
  switch (ext) {
    case ".js":
      contentType = "text/javascript";
      break;
    case ".css":
      contentType = "text/css";
      break;
    default:
      contentType = "text/html";
      break;
  }
  fs.readFile(filePath, (err, data) => {
    if (err) {
      console.log(err);
      if (err.code === "ENOENT") {
        res.writeHead(404, { "Content-Type": "text/html" });
        return res.end("File not found", "utf-8");
      }

      res.writeHead(500, { "Content-Type": "text/html" });
      return res.end("Unknown Internal Error", "utf-8");
    }

    res.writeHead(200, { "Content-Type": contentType });
    return res.end(data, "utf-8");
  });
});
server.on("clientError", (err, socket) => {
  socket.end("HTTP/1.1 400 Bad Request\r\n\r\n");
});

const wss = new Server({ server });

wss.on("connection", (ws: WS) => {
  // generate a game ID to be used to read/write into the game store
  let game: Game;
  console.log(`New connection`);

  //listen of messages on the WebSocket
  ws.on("message", (message: string) => {
    //parse messages into JSON as incomingMessage type
    const msg = JSON.parse(message) as inOutMessage;

    //switch through possible msg types
    switch (msg.msg) {
      case "NODE_CLICKED":
        // console.log(msg);

        // Run node click function and send its response to client
        ws.send(nodeClick(game, msg));
        // console.table(game.map.map(map => map.map(point => point.isSelected)));
        break;
      case "INITIALIZE":
        // console.log("INIT\n", msg);
        // Generate new game and store it to WS session scope
        game = store.generateGame();
        console.log(`Generating object with ID ${game.id}`);
        // Send OK
        return ws.send(
          game.generateResponse(
            msg.id,
            "INITIALIZE",
            "Waiting for player 1 to make a move"
          )
        );
      case "ERROR":
        // Log any errors
        return console.log(msg);
      default:
        // Log any other WS msg types that are unknown
        console.log("This technically shouldn't happen\n", msg);
        break;
    }
  });
});

// Listen on port
server.listen(PORT, () => {
  console.log(`LISTENING ON POINT ${PORT}`);
});

export type inOutMessage = {
  id: number;
  msg:
    | "STATE_UPDATE"
    | "POINT"
    | "STRING"
    | "INITIALIZE"
    | "NODE_CLICKED"
    | "VALID_START_NODE"
    | "INVALID_START_NODE"
    | "ERROR";
  body: null | stateUpdate | point | string;
};

export type point = {
  x: number;
  y: number;
};

export type line = {
  start: point;
  end: point;
};

export type stateUpdate = {
  newLine: line | null;
  heading: string | null;
  message: string | null;
};
