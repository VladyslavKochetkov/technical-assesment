import { Game } from "./game";
const findIntersections = require("bentley-ottman-sweepline");

export class Point {
  // Easier to access members without object wrapper
  private x: number = -1;
  private y: number = -1;

  // Game gets passed by ref for easy access
  private game: Game;
  constructor(x: number, y: number, game: Game) {
    this.x = x;
    this.y = y;
    this.game = game;
  }

  // Toggle for if node is selected
  public isSelected: boolean = false;

  // Returns cords in point object
  public getCords(): {
    x: number;
    y: number;
  } {
    return {
      x: this.x,
      y: this.y
    };
  }

  // Select node
  public select(): void {
    this.isSelected = true;
  }

  // Do calculation to get angel degree from this point to passed point
  // Do addition math to make future calculations easier such as normalize axis
  public getDegree(x: number, y: number): number {
    return -((Math.atan2(this.y - y, this.x - x) * 180) / Math.PI - 180) % 360;
  }

  // Return false is no more points
  // Return true if there are more points
  public checkAllPointsAround(): boolean {
    // Array of all surrounding nodes
    const possibility = [
      [this.x + 1, this.y + 1],
      [this.x + 1, this.y],
      [this.x + 1, this.y - 1],
      [this.x, this.y + 1],
      [this.x, this.y - 1],
      [this.x - 1, this.y + 1],
      [this.x - 1, this.y],
      [this.x - 1, this.y - 1]
    ]
      .filter(
        // Remove nodes that are out of bound
        point => point.filter(cord => cord <= 3 && cord >= 0).length === 2
      )
      // Remove nodes that are selected already as they cant be visited
      .filter(point => !this.game.getPoint(point[0], point[1]).isSelected);

    // If there are no more possible nodes, return false because no more possible moves from this stop
    if (possibility.length === 0) return false;

    for (const point of possibility) {
      // Make an array to test with for line intersections, this array is all past line segments
      // and one of the possibility, with negatives maped for easier visuals because if we
      // start from top on regular graph, going down generally would provide a negative value
      const test = [
        ...this.game.lineSegments,
        [[this.x, -this.y], [point[0], -point[1]]]
      ];

      // Use Bentley Ottman sweeping algorithm to detect if there is an overlap, in this case it should
      // always be on a _.5 location
      const intersectionInTest = findIntersections(test).filter(
        // Filter all points away that are not decimal
        (point: [number, number]) => point[0] % 1 !== 0 || point[1] % 1 !== 0
      );
      // If there are no decimal that means that there is still a possible point left
      if (intersectionInTest.length === 0) return true;
    }
    // Falls back to false because all nodes have a decimal (meaning would intersect)
    return false;
  }
}
