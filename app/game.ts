import { Point } from "./point";

// Map should be private, all point should be accessed via (x,y) functions
export class Game {
  // Generate a random game ID
  public id: number = Math.floor(Math.random() * 100000);
  // Store nodes in 2D array of points
  private map: Array<Array<Point>> = [];
  constructor() {
    // Generate an X,Y array plot to keep the points
    for (let x = 0; x < 4; x++) {
      this.map[x] = [];
      for (let y = 0; y < 4; y++) this.map[x][y] = new Point(x, y, this);
    }
  }

  // Which node should be replaces, this depends on which node is clicked on, tail or head
  public toReplace: "startNode" | "endNode" = "startNode";

  // Start and end node
  public startNode: Point | undefined;
  public endNode: Point | undefined;

  // Last selected node
  public selectedNode: Point | undefined;

  // Keep track of player turns
  public turn: 1 | 2 = 1;

  // Store line segments that are generated in order to check for oberlaps
  public lineSegments: Array<Array<Array<number>>> = [];

  // Return the point base on (x,y)
  public getPoint(x: number, y: number): Point {
    return this.map[x][y];
  }
  // Toggle turns between 1 & 2
  public nextTurn(): 1 | 2 {
    this.turn = this.turn === 1 ? 2 : 1;
    return this.turn;
  }
  // Select a node
  public selectNode(x: number, y: number, dontVerify: boolean = false): Point {
    const point = this.getPoint(x, y);
    if (point.isSelected && dontVerify) throw "Point has already been selected";
    point.select();
    this.selectedNode = point;
    return point;
  }

  // Generate a server response, as string for WS to send
  public generateResponse(
    id: number,
    msg: messageTypes,
    bodyMsg: string | null,
    newLine: object | null = null
  ): string {
    const res = {
      id,
      msg,
      body: {
        newLine: newLine,
        heading: `Player ${this.turn}`,
        message: bodyMsg
      }
    };
    // console.log(res);
    return JSON.stringify(res);
  }
}

type messageTypes =
  | "VALID_START_NODE"
  | "INVALID_START_NODE"
  | "VALID_END_NODE"
  | "INVALID_END_NODE"
  | "INITIALIZE"
  | "VALID_END_NODE"
  | "GAME_OVER";
