import { Game } from "./game";

// Stores make games
class Store {
  // Store all games in a map [hash map]
  private games: {
    [key: string]: Game;
  } = {};

  // Returns a game, but this really shouldn't be used as games should be stored in connection session
  public getGame = (id: number): Game => this.games[id];

  // Generate game and save to map
  public generateGame = (): Game => {
    const game = new Game();
    this.games[game.id] = game;
    return game;
  };

  // Delete specific key
  public deleteGame = (id: number): void => {
    delete this.games[id];
  };

  // Delete all games by generating new games object
  public deleteAll = () => {
    this.games = {};
  };
}

export const store = new Store();
